package io.bitservices.batttemp.control

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.text.format.DateUtils
import io.bitservices.batttemp.data.BatteryTemperature
import io.bitservices.batttemp.data.ChargingStatus

class BatteryStatistics {
    companion object {
        private const val BATTERY_TEMP_DIVISOR: Float = 10f
        private const val CACHE_TIMEOUT: Float = 9.5f

        private var lastUpdate: Long = 0

        private var temperature: Float? = null
        private var isCharging: Boolean? = null

        fun batteryTemp(context: Context): BatteryTemperature {
            checkBattery(context)
            return(BatteryTemperature(temperature))
        }

        fun batteryIsCharging(context: Context): ChargingStatus {
            checkBattery(context)
            return(ChargingStatus(isCharging))
        }

        private fun checkBattery(context: Context) {
            if ((lastUpdate + (DateUtils.SECOND_IN_MILLIS * CACHE_TIMEOUT)) < System.currentTimeMillis()) {
                // The last update was too long ago and we need to refresh battery data.
                refreshBattery(context)
            }
        }

        private fun refreshBattery(context: Context) {
            var refreshTemperature: Float? = null
            var refreshIsCharging: Boolean? = null

            // Attempt to read from Battery Manager.
            val intent: Intent? = context.registerReceiver(null, IntentFilter(Intent.ACTION_BATTERY_CHANGED))

            if (intent != null) {
                // Set up error values
                val errorTemperature: Int = (-274 * BATTERY_TEMP_DIVISOR).toInt()
                val errorIsCharging: Int = -1

                // Get Battery Temperature
                val intentTemperature: Int = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, errorTemperature)

                if (intentTemperature != errorTemperature) {
                    refreshTemperature = intentTemperature.toFloat() / BATTERY_TEMP_DIVISOR
                }

                val intentIsCharging: Int = intent.getIntExtra(BatteryManager.EXTRA_STATUS, errorIsCharging)

                if (intentIsCharging != errorIsCharging) {
                    refreshIsCharging = intentIsCharging == BatteryManager.BATTERY_STATUS_CHARGING ||
                                        intentIsCharging == BatteryManager.BATTERY_STATUS_FULL
                }
            }

            temperature = refreshTemperature
            isCharging = refreshIsCharging
        }
    }
}