package io.bitservices.batttemp.control

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.text.format.DateUtils
import io.bitservices.batttemp.MainService
import io.bitservices.batttemp.R

class ServiceManager {
    companion object {
        private const val SERVICE_UPDATE_INTERVAL: Long = DateUtils.MINUTE_IN_MILLIS

        private var am: AlarmManager? = null
        private var intent: Intent? = null
        private var pIntent: PendingIntent? = null
        private var sp: SharedPreferences? = null

        fun startService(context: Context) {
            getSharedPreferences(context)
                .edit()
                .putBoolean(context.getString(R.string.shared_prefs_boot_run_setting), true)
                .apply()

            context.startService(getIntent(context))
            getAlarmManager(context).setRepeating(AlarmManager.RTC, System.currentTimeMillis(), SERVICE_UPDATE_INTERVAL, getPendingIntent(context))
        }

        fun stopService(context: Context) {
            getSharedPreferences(context)
                .edit()
                .putBoolean(context.getString(R.string.shared_prefs_boot_run_setting), false)
                .apply()

            getAlarmManager(context).cancel(getPendingIntent(context))
            context.stopService(getIntent(context))
        }

        private fun getAlarmManager(context: Context): AlarmManager {
            if (am == null) {
                am = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            }

            return am!!
        }

        private fun getIntent(context: Context): Intent {
            if (intent == null) {
                intent = Intent(context, MainService::class.java)
            }

            return intent!!
        }

        private fun getPendingIntent(context: Context): PendingIntent {
            if (pIntent == null) {
                pIntent = PendingIntent.getService(context, 0, getIntent(context), PendingIntent.FLAG_UPDATE_CURRENT + PendingIntent.FLAG_IMMUTABLE)
            }

            return pIntent!!
        }

        private fun getSharedPreferences(context: Context): SharedPreferences {
            if (sp == null) {
                sp = context.getSharedPreferences(context.getString(R.string.shared_prefs_boot_settings), Context.MODE_PRIVATE)
            }

            return sp!!
        }
    }
}