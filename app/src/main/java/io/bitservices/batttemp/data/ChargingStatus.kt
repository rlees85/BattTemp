package io.bitservices.batttemp.data

data class ChargingStatus (
    private val rawIsCharging: Boolean?,

    // Presume we are charging, if we don't know. To be safe...
    val isCharging: Boolean = rawIsCharging ?: true,
    val isKnown: Boolean = rawIsCharging != null
) {
    override fun toString(): String {
        return if (isKnown) {
            if (isCharging) {
                "Charging"
            } else {
                "Discharging"
            }
        } else {
            "Unknown"
        }
    }

    fun isDifferent(to: ChargingStatus?): Boolean {
        if (to == null) {
            return true
        }

        return to.isCharging != isCharging
    }
}