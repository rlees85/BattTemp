package io.bitservices.batttemp.data

import androidx.annotation.ColorInt
import io.bitservices.batttemp.R
import kotlin.math.abs

class BatteryTemperature (val temperature: Float?) {

    val colour: Int
    val status: String
    val chargeStatus: String

    val criticalDrawable: Int?

    private val isCritical: Boolean

    init {
        this.colour = colour()
        this.status = status()
        this.chargeStatus = chargeStatus()
        this.isCritical = temperature != null && temperature >= CRITICAL_BATTERY_TEMPERATURE
        this.criticalDrawable = criticalDrawable()
    }

    override fun toString(): String {
        return if (temperature != null) "$temperature°C" else "Unknown Temperature"
    }

    fun isDifferent(to: BatteryTemperature?, difference: Float): Boolean {
        // Always return true if comparing to a null BatteryTemperature (first update)
        if (to == null) {
            return true
        }

        // Always return true if the "known" status is different between the two.
        if ((temperature != null) != (to.temperature != null)) {
            return true
        }

        // Return true if the temperatures are known and have a larger or equal
        // difference to "difference".
        return temperature != null &&
                to.temperature != null &&
                abs(to.temperature - this.temperature) >= difference
    }

    fun drawable(chargingStatus: ChargingStatus): Int {
        if (temperature != null ) {
            return when {
                temperature >= 47.5f -> if (chargingStatus.isCharging) R.drawable.p50c else R.drawable.p50
                temperature >= 42.5f -> if (chargingStatus.isCharging) R.drawable.p45c else R.drawable.p45
                temperature >= 37.5f -> R.drawable.p40
                temperature >= 32.5f -> R.drawable.p35
                temperature >= 27.5f -> R.drawable.p30
                temperature >= 22.5f -> R.drawable.p25
                temperature >= 17.5f -> R.drawable.p20
                temperature >= 12.5f -> R.drawable.p15
                temperature >= 7.5f -> R.drawable.p10
                temperature >= 2.5f -> R.drawable.p5
                temperature >= -2.5f -> if (chargingStatus.isCharging) R.drawable.p0c else R.drawable.p0
                temperature >= -7.5f -> if (chargingStatus.isCharging) R.drawable.m5c else R.drawable.m5
                else -> if (chargingStatus.isCharging) R.drawable.m10c else R.drawable.m10
            }
        } else {
            return R.drawable.e
        }
    }

    private fun criticalDrawable(): Int? {
        return if (isCritical) R.drawable.critical else null
    }

    @ColorInt
    private fun colour(): Int {
        return when {
            temperature == null || temperature >= 50f -> R.color.gaugeRed
            temperature >= 45f -> R.color.gaugeOrange
            temperature >= 35f -> R.color.gaugeYellow
            temperature >= 15f -> R.color.gaugeGreen
            temperature > 5f -> R.color.gaugeLightBlue
            temperature > 0f -> R.color.gaugeBlue
            else -> R.color.gaugePurple
        }
    }

    private fun status(): String {
        return if (temperature != null) {
            when {
                temperature >= 50f -> "Overheat"
                temperature >= 45f -> "Hot"
                temperature >= 35f -> "Warm"
                temperature >= 15f -> "Optimal"
                temperature > 5f -> "Cool"
                temperature > 0f -> "Cold"
                else -> "Freezing"
            }
        } else {
            "Unknown"
        }
    }

    private fun chargeStatus(): String {
        return if (temperature != null) {
            when {
                temperature >= 50f -> "Cool or power off now!"
                temperature >= 45f -> "Cool before charging."
                temperature >= 35f -> "Cooler charging recommended."
                temperature >= 15f -> "Charging recommended."
                temperature > 5f -> "Warmer charging recommended."
                temperature > 0f -> "Warm before charging."
                else -> "Frozen, do not charge!"
            }
        } else {
            "Unknown temperature."
        }
    }

    companion object {
        private const val CRITICAL_BATTERY_TEMPERATURE: Float = 50f
    }
}