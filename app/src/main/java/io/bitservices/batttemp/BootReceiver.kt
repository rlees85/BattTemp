package io.bitservices.batttemp

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import io.bitservices.batttemp.control.ServiceManager

class BootReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        // On boot.

        if(context != null &&
            intent != null &&
            intent.action == Intent.ACTION_BOOT_COMPLETED) {

            // Check if we have the service set to enabled.
            val sp: SharedPreferences = context.getSharedPreferences(context.getString(R.string.shared_prefs_boot_settings), Context.MODE_PRIVATE)
            val run: Boolean = sp.getBoolean(context.getString(R.string.shared_prefs_boot_run_setting), false)

            // If it is enabled
            if (run) {
                // Start it!
                ServiceManager.startService(context)
            }
        }
    }
}