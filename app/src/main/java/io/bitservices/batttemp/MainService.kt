package io.bitservices.batttemp

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.graphics.PixelFormat
import android.os.IBinder
import android.view.WindowManager
import android.view.WindowManager.LayoutParams
import android.widget.ImageView
import io.bitservices.batttemp.control.BatteryStatistics
import io.bitservices.batttemp.data.BatteryTemperature
import io.bitservices.batttemp.data.ChargingStatus


class MainService : Service() {
    private val nId: Int = 1

    private lateinit var nMan: NotificationManager
    private lateinit var nBuild: Notification.Builder

    private lateinit var wm: WindowManager
    private lateinit var wmView: ImageView
    private lateinit var wmLayoutParams: LayoutParams

    private var lastTemperature: BatteryTemperature? = null
    private var lastChargingStatus: ChargingStatus? = null

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        // Setup parent class (Service)
        super.onCreate()

        // Setup Critical Temperature Overlay
        wmView = ImageView(applicationContext)
        wmLayoutParams = LayoutParams(512, 512, LayoutParams.TYPE_APPLICATION_OVERLAY, LayoutParams.FLAG_LAYOUT_IN_SCREEN or LayoutParams.FLAG_NOT_TOUCHABLE, PixelFormat.TRANSLUCENT)
        wm = getSystemService(WINDOW_SERVICE) as WindowManager

        // Setup Notification Manager
        nMan = getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        // Setup Notification Channel
        val ncId: String = applicationContext.packageName + "." + applicationContext.getString(R.string.notification_channel)
        val ncName: CharSequence = applicationContext.getString(R.string.app_name)
        val ncDescription: String = applicationContext.getString(R.string.notification_description)
        val nc: NotificationChannel = NotificationChannel(ncId, ncName, NotificationManager.IMPORTANCE_LOW)
        nc.description = ncDescription
        nc.setShowBadge(false)
        nMan.createNotificationChannel(nc)

        // Setup Notification Intent
        val nPendingIntent: PendingIntent = PendingIntent.getActivity(applicationContext, 0, Intent(applicationContext, MainActivity::class.java), PendingIntent.FLAG_IMMUTABLE)

        // Build Notification
        nBuild = Notification.Builder(applicationContext, ncId)
            .setContentTitle(ncName)
            .setContentIntent(nPendingIntent)
            .setOnlyAlertOnce(true)
            .setOngoing(true)
    }

    override fun onDestroy() {
        // Destroy Critical Temperature Overlay
        try {
            if (wmView.parent != null) {
                wm.removeView(wmView)
            }
        } catch (_: Exception){}

        // Destroy Notification
        try {
            nMan.cancel(nId)
        } catch (_: Exception){}

        // Destroy last values
        lastTemperature = null
        lastChargingStatus = null

        // Destroy parent class (Service)
        super.onDestroy()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        // Refresh the battery temperature
        val currentTemperature: BatteryTemperature = BatteryStatistics.batteryTemp(applicationContext)

        // Refresh the charge status
        val currentChargingStatus: ChargingStatus = BatteryStatistics.batteryIsCharging(applicationContext)

        // Check if we care enough to re-post the notification
        if (currentTemperature.isDifferent(lastTemperature, MINIMUM_DIFFERENCE) || currentChargingStatus.isDifferent(lastChargingStatus)) {
            // Update the previous status with what is now current
            lastTemperature = currentTemperature
            lastChargingStatus = currentChargingStatus

            // Start notification
            nBuild.setWhen(System.currentTimeMillis())
                .setSmallIcon(currentTemperature.drawable(currentChargingStatus))
                .setContentText(currentTemperature.toString() + " - " + currentTemperature.status)
                .setColor(getColor(currentTemperature.colour))

            nMan.notify(nId, nBuild.build())
        }

        // Check if the temperature is now critical
        if (currentTemperature.criticalDrawable != null) {
            // It is now critical.
            if (wmView.parent == null) {
                wmView.setImageResource(currentTemperature.criticalDrawable)
                wm.addView(wmView, wmLayoutParams)
            }
        } else {
            // It is no longer critical (if it was).
            if (wmView.parent != null) {
                wm.removeView(wmView)
            }
        }

        // Stop the service when complete.
        return START_NOT_STICKY
    }

    companion object {
        private const val MINIMUM_DIFFERENCE: Float = 1f
    }
}