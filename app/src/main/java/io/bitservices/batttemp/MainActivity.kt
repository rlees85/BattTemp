package io.bitservices.batttemp

import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.text.format.DateUtils
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.appcompat.app.AppCompatActivity
import com.github.anastr.speedviewlib.SpeedView
import com.github.anastr.speedviewlib.components.Section
import io.bitservices.batttemp.control.BatteryStatistics
import io.bitservices.batttemp.control.ServiceManager
import io.bitservices.batttemp.data.BatteryTemperature
import java.util.Timer
import java.util.TimerTask

class MainActivity : AppCompatActivity() {
    private val handler: Handler = Handler(Looper.getMainLooper())

    private var timer: Timer = Timer()

    private lateinit var svBatteryTemperature: SpeedView

    private lateinit var tvChargingStatus: TextView
    private lateinit var tvVersion: TextView

    @ColorInt
    private var errorColor: Int = 0
    @ColorInt
    private var gaugeColor: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.setBackgroundDrawable(ColorDrawable(getColor(R.color.primaryDark)))

        // Setup gauge controls (colour bands, etc)
        svBatteryTemperature = findViewById<SpeedView>(R.id.svBatteryTemperature)
        svBatteryTemperature.clearSections()
        svBatteryTemperature.addSections(
            Section(0f, .25f, getColor(R.color.gaugePurple), svBatteryTemperature.speedometerWidth),
            Section(.25f, .3125f, getColor(R.color.gaugeBlue), svBatteryTemperature.speedometerWidth),
            Section(.3125f, .4375f, getColor(R.color.gaugeLightBlue), svBatteryTemperature.speedometerWidth),
            Section(.4375f, .6875f, getColor(R.color.gaugeGreen), svBatteryTemperature.speedometerWidth),
            Section(.6875f, .8125f, getColor(R.color.gaugeYellow), svBatteryTemperature.speedometerWidth),
            Section(.8125f, .875f, getColor(R.color.gaugeOrange), svBatteryTemperature.speedometerWidth),
            Section(.875f, 1f, getColor(R.color.gaugeRed), svBatteryTemperature.speedometerWidth)
        )

        // Setup text fields
        tvChargingStatus = findViewById<TextView>(R.id.tvChargingStatus)
        tvVersion = findViewById<TextView>(R.id.tvVersion)
        tvVersion.text = BuildConfig.VERSION_NAME

        // Check Overlay Permission
        if (!Settings.canDrawOverlays(this)) {
            val overlayPermissionIntent: Intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION)
            overlayPermissionIntent.setData(Uri.parse("package:" + packageName))
            startActivityForResult(overlayPermissionIntent, 101)
        }

        // Setup base colours
        errorColor = getColor(R.color.textColorError)
        gaugeColor = svBatteryTemperature.speedTextColor
    }

    override fun onResume() {
        super.onResume()
        startTimer()
    }

    override fun onPause() {
        super.onPause()
        stopTimer()
    }

    override fun onStop() {
        super.onStop()
        stopTimer()
    }

    override fun onDestroy() {
        super.onDestroy()
        stopTimer()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.mnuStart -> ServiceManager.startService(this)
            R.id.mnuStop -> ServiceManager.stopService(this)
        }

        return super.onOptionsItemSelected(item)
    }

    private fun refresh() {
        handler.post {
            val temperature: BatteryTemperature = BatteryStatistics.batteryTemp(this)

            // Update the gauge
            if (temperature.temperature != null) {
                // Move to new value if temperature is known
                gaugeSet(svBatteryTemperature, temperature.temperature)
            } else {
                // Stay still and go red if not known
                gaugeError(svBatteryTemperature)
            }

            // Build and apply the status string
            val status: StringBuilder = StringBuilder()
                .append(temperature.status)
                .append(" - ")
                .append(temperature.chargeStatus)

            tvChargingStatus.text = status.toString()

            // Update the activity colour scheme based on temperature.
            supportActionBar?.setBackgroundDrawable(ColorDrawable(getColor(temperature.colour)))
            window.statusBarColor = getColor(temperature.colour)
        }
    }
    private fun startTimer() {
        stopTimer()

        val timerTask: TimerTask = object:TimerTask() {
            override fun run() {
                refresh()
            }
        }

        timer = Timer()
        timer.schedule(timerTask, 0, UI_REFRESH_INTERVAL)
    }

    private fun stopTimer() {
        timer.cancel()
    }

    companion object {
        private const val UI_REFRESH_INTERVAL: Long = DateUtils.SECOND_IN_MILLIS * 10
    }

    private fun gaugeSet(sv: SpeedView, speed: Float) {
        sv.speedTextColor = gaugeColor
        sv.textColor = gaugeColor
        sv.unitTextColor = gaugeColor
        sv.speedTo(speed)
    }
    private fun gaugeError(sv: SpeedView) {
        sv.speedTextColor = errorColor
        sv.textColor = errorColor
        sv.unitTextColor = errorColor
    }
}